#!/bin/bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export DQ2_LOCAL_SITE_ID=DESY-HH_SCRATCHDISK 
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh

#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
#source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
  
#asetup 17.2.11.14,AtlasProduction,64,slc5
#asetup 19.2.0
#asetup 19.3.0
#asetup 19.2.5.1,here
#asetup 19.2.4.12.1,MCProd,64
asetup AthGeneration,21.6.29, here
###On lxplus
#setupATLAS
#asetup 19.2.0
export RIVET_ANALYSIS_PATH=$PWD     
#export LHAPATH=/afs/.cern.ch/sw/lcg/external/MCGenerators_lcgcmt67b/lhapdf/5.9.1/share/lhapdf/PDFsets

