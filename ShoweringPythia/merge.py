import os,sys
from itertools import cycle, islice, dropwhile
import ROOT                                    

def save_histogram_root(hist, filepath, mode='RECREATE'):
    fout = ROOT.TFile(filepath, mode)
    hist.Write(hist.GetName())
    print (mode)
    fout.Close()
                                           
GENS = ["PhPy8"]
#Tunes =['ATLASA14NNPDF']                                                                                                                                                                                  
#Tunes = ["ATLASA14NNPDFEIG3bp", "ATLASA14NNPDFEIG3bn", "ATLASA14NNPDFEIG3cp", "ATLASA14NNPDFEIG3cn"]                                                                                                       
#Effects = ['Hadr','UE']#,'UE+Had']                                                                                                                                                                         
Effects = ['UEHad']

JS = [1,2,3,4,5,6,7,8,9]

hists   = ["Matrix_geom",
           "Matched_parton_jets","All_parton_jets","PartonJetsY0","PartonJetsY1","PartonJetsY2","PartonJetsY3","PartonJetsY4","PartonJetsY5","PartonJetsY6","PartonJetsY7","PartonJetsY8",
           "Matched_hadron_jets","All_hadr_jets","HadronJetsY0","HadronJetsY1","HadronJetsY2","HadronJetsY3","HadronJetsY4","HadronJetsY5","HadronJetsY6","HadronJetsY7","HadronJetsY8"]

isNorm = True
isMerge = True

if isNorm:
    for GEN in GENS:
        for Effect in Effects:
            for J in JS:
                savedir = "/nfs/dust/atlas/user/star/Mikhalcov/Powheg_pt2/MatchingOutputResultWithRapidity/%s/merged/Samples/" % (Effect)
                filecounter = 0
                command = "ls -l /nfs/dust/atlas/user/star/Mikhalcov/Powheg_pt2/MatchingOutputResultWithRapidity/%s/*%s_*%s*_JS%i_*| wc -l" % (Effect,GEN,Effect,J)
                os.system(command)
                filecounter = os.popen(command).read()
                print (filecounter)
                                MergedFileName = "%s%s_JS%i.root" % (savedir,GEN,J)
                print (MergedFileName)
                command = "hadd -f %s /nfs/dust/atlas/user/star/Mikhalcov/Powheg_pt2/MatchingOutputResultWithRapidity/%s/*%s_*%s*_JS%i_*" % (MergedFileName,Effect,GEN,Effect,J)
                print (command)
                os.system(command)  ############# Raskommentit                                                                                                                                                                        
                fj = ROOT.TFile.Open(MergedFileName,"READ")
                print (fj)
                fj.Print()
                isFileRec = False
		        for hist in hists:
                    print (hist)
                    htmp = fj.Get(hist)
                    htmp.Scale(1/float(filecounter))
                    #print (htmp)                                                                                                                                                                           
                    htmp.Print()
                    if (isFileRec):
                        save_histogram_root(htmp,MergedFileName[:-5]+"_normed.root","UPDATE")
                    else:
                        save_histogram_root(htmp,MergedFileName[:-5]+"_normed.root")
                        isFileRec=True
                fj.Close()


if isMerge:
    for GEN in GENS:
        for Effect in Effects:
            dirwithnorm = "/nfs/dust/atlas/user/star/Mikhalcov/Powheg_pt2/MatchingOutputResultWithRapidity/%s/merged/Samples/" % (Effect)
            command = "hadd -f %s %s%s_*normed*" % (dirwithnorm[:-8]+GEN+".root",dirwithnorm,GEN)
            print (command)
            os.system(command)
