#/!bin/bash                                                                                                                                                                                                 
let "JS = 1"
let "seed = 0"
#search_dir=/nfs/dust/atlas/user/star/Mikhalcov/user.smikhalc.Ph_LHE_100000_JS1.vtest_EXT0/                                                                                                                 

for entry in `ls $search_dir*lhe*`;
do
#    rucio download user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_Parton_${seed}.vtest_EXT0                                                                                                          
#    rucio download user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_Hadr_${seed}.vtest_EXT0                                                                                                            
#    rucio download user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_UE_${seed}.vtest_EXT0                                                                                                              
#    rucio download user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_UEHad_${seed}.vtest_EXT0                                                                                                           
#    rucio download user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_NOShower_${seed}.vtest_EXT0                                                                                                        
#    echo "user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_Parton_${seed}.vtest_EXT0"                                                                                                                  
    if [ ! -e "user.smikhalc.PhPy8_LHE_100000_JS${JS}_Parton_${seed}_massPr_2.vtest_EXT0" ] ##check if file aready exist
    then
        rucio download user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_Parton_${seed}_massPr_2.vtest_EXT0 ### if no download from rucio 
        #pathena --trf "Generate_tf.py --ecmEnergy=13000. --maxEvents=100000 --runNumber=10000 --firstEvent=1 --randomSeed=${JS}${seed} --outputEVNTFile=%OUT.pool.root --jobConfig=mc.Py8_A14NNPDF23_jj.py\
 --inputGeneratorFile=_1 --env NPC_EFFECT=Parton" --extFile=RivetJETS.so,MC15JobOptions,${entry} --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_Parton_${seed}_Pr\
od_W_v1.vtest  ### or you can send to PanDa                                                                                                                                                                                              
        #pathena --trf "Gen_tf.py --jobConfig=100000 --ecmEnergy=13000 --randomSeed=${JS}${seed} --maxEvents=100000 --inputGeneratorFile=lhe --outputEVNTFile=%OUT.pool.root --env NPC_EFFECT=Parton" --ext\
File=RivetJETS.so,${entry} --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_Parton_${seed}_norm.vtest;                                                              
    fi
    #if [ ! -e "user.smikhalc.PhPy8_LHE_100000_JS${JS}_NOShower_${seed}.vtest_EXT0" ]                                                                                                                       
    #then                                                                                                                                                                                                   
        #rucio download user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_NOShower_${seed}_norm.vtest_EXT0                                                                                               
        #pathena --trf "Gen_tf.py --jobConfig=100000 --ecmEnergy=13000 --randomSeed=${JS}${seed} --maxEvents=100000 --inputGeneratorFile=lhe --outputEVNTFile=%OUT.pool.root --env NPC_EFFECT=NOShower" --e\
xtFile=RivetJETS.so,$entry --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_NOShower_${seed}_norm.vtest;                                                            
    #fi                                                                                                                                                                                                     
    #if [ ! -e "user.smikhalc.PhPy8_LHE_100000_JS${JS}_UE_${seed}.vtest_EXT0" ]                                                                                                                             
    #then                                                                                                                                                                                                   
        #rucio download user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_UE_${seed}_norm.vtest_EXT0                                                                                                     
        #pathena --trf "Gen_tf.py --jobConfig=100000 --ecmEnergy=13000 --randomSeed=${JS}${seed} --maxEvents=100000 --inputGeneratorFile=lhe --outputEVNTFile=%OUT.pool.root --env NPC_EFFECT=UE" --extFile\
=RivetJETS.so,$entry --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_UE_${seed}_norm.vtest;                                                                        
    #fi                                                                                                                                                                                                     
    if [ ! -e "user.smikhalc.PhPy8_LHE_100000_JS${JS}_UEHad_${seed}_massPr_2.vtest_EXT0" ]
    then
        rucio download user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_UEHad_${seed}_massPr_2.vtest_EXT0
        #pathena --trf "Generate_tf.py --ecmEnergy=13000. --maxEvents=100000 --runNumber=10000 --firstEvent=1 --randomSeed=${JS}${seed} --outputEVNTFile=%OUT.pool.root --jobConfig=mc.Py8_A14NNPDF23_jj.py\
 --inputGeneratorFile=_1 --env NPC_EFFECT=UEHad" --extFile=RivetJETS.so,MC15JobOptions,${entry} --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_UEHad_${seed}_Prod\
_W_v1.vtest                                                                                                                                                                                                 
        #pathena --trf "Gen_tf.py --jobConfig=100000 --ecmEnergy=13000 --randomSeed=${JS}${seed} --maxEvents=100000 --inputGeneratorFile=lhe --outputEVNTFile=%OUT.pool.root --env NPC_EFFECT=UE+Had" --ext\
File=RivetJETS.so,$entry --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_UEHad_${seed}_norm.vtest;                                                                 
    fi
    #if [ ! -e "user.smikhalc.PhPy8_LHE_100000_JS${JS}_Hadr_${seed}.vtest_EXT0" ]                                                                                                                           
    #then                                                                                                                                                                                                   
        #rucio download user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_Hadr_${seed}_norm.vtest_EXT0                                                                                                   
        #pathena --trf "Gen_tf.py --jobConfig=100000 --ecmEnergy=13000 --randomSeed=${JS}${seed} --maxEvents=100000 --inputGeneratorFile=lhe --outputEVNTFile=%OUT.pool.root --env NPC_EFFECT=Hadr" --extFi\
le=RivetJETS.so,$entry --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_Hadr_${seed}_norm.vtest;                                                                    
    #fi                                                                                                                                                                                                     
    let "seed = seed + 1"
done
