#!/bin/bash

echo "----------------------------------------------------------------"
echo "Start: "$(date)
echo "on host: "${HOSTNAME}
echo "----------------------------------------------------------------"

echo ${PWD}
ls -lsa

EVENTS=${1}
R=0.400000
RUNDIR=${2}
STEP=${3}

#echo "ECM = "${ECM}
echo "NPC_GEN = "${NPCGEN}
echo "NPC_EFFECT = "${NPCEFFECT}
echo "EVENTS = "${EVENTS}
echo "JSAMPLE = "${NPCJSAMPLE}

source setup.sh
lsetup root
lsetup "root 6.18.04-x86_64-centos7-gcc8-opt"

root --rootVer=6.18/04 --cmtConfig=x86_64-centos7-gcc8-opt -l -b -q Matching.C\(\"/nfs/dust/atlas/user/star/Mikhalcov/TestPowhegWeights/user.smikhalc.Ph_LHE_${EVENTS}_JS${NPCJSAMPLE}.vtest_EXT0/user.smikhalc.${NPCGEN}_LHE_${EVENTS}_JS${NPCJSAMPLE}_\",\"${NPCEFFECT}_${STEP}_Prod_W.vtest_EXT0/TreeJets.root\",\"Parton_${STEP}_Prod_W.vtest_EXT0/TreeJets.root\",\"result_GEN_${NPCGEN}_EFFECT_${NPCEFFECT}_Events_${EVENTS}_JS${NPCJSAMPLE}_${STEP}.root\",${R},\"/nfs/dust/atlas/user/star/Mikhalcov/TestPowhegWeights/MatchingOutputResultWithRapidity/${NPCEFFECT}/matrix_R_${R}_GEN_${NPCGEN}_EFFECT_${NPCEFFECT}_Events_${EVENTS}_JS${NPCJSAMPLE}_${STEP}.root\"\)

echo "----------------------------------------------------------------"
echo "Finish: "$(date)
echo "on host: "${HOSTNAME}
echo "----------------------------------------------------------------"

