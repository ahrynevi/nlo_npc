// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/FastJets.hh"

#include "Rivet/Particle.fhh"

#include "TH1D.h"
#include "TFile.h"
#include "TTree.h"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/JetAlg.hh"
//#include "Rivet/Projections/IdentifiedFinalState.hh"
//#include "Rivet/Projections/LeadingParticlesFinalState.hh"

#include <fstream>
#include <vector>
#include <sstream>
#include <string> 
///

namespace Rivet{

  // const int nAlg = 1;
  // const string AlgName[nAlg] = { "AKT04" };
  // const double R      [nAlg] = { 0.4 };

  const int nAlg = 2;
  const string AlgName[nAlg] = { "AKT04", "AKT06"};
  const double R      [nAlg] = { 0.4,     0.6 };

  const int nybins = 7;
  const string yBinsNames[nybins]   = { "0", "1",  "2",  "3",  "4",  "5",  "6" };
  const double yBinsEdges[nybins+1] = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 5.0 };
  const double yStarBins [nybins+1] = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 5.0 };
                                                                                                                                              


  class JETS : public Analysis 
  {
    
  public:
    
    JETS() : Analysis("LeadingJets") { }
        
  private:
    
    TFile *file;

    TTree *tree;
    
    TH1D *h_nFiles;
    TH1D *h_xs;
    TH1D *h_nEvents;
    TH1D *h_sumOfWeights;
    TH1D *h_genFiltEff;
 
    int *jet_N;
    std::vector<double> *jet_E;                                                                                                                      
    std::vector<double> *jet_Pt;                                                                                                                      
    std::vector<double> *jet_M;                                                                                                                      
    std::vector<double> *jet_Rap;                                                                                                                     
    std::vector<double> *jet_Eta;                                                                    
    std::vector<double> *jet_Phi;

    long   evt;
    double sumw;
    
    int eventNumber;
    double eventWeight;
    double getFiltEff;




  public:
    
    float getXSfromLog()
    {
      cout << "getting cross-section from log" << endl;
      string cfilename="log.generate";
      std::ifstream fileInput;
      fileInput.open(cfilename.c_str());
      char* search = "MetaData: cross-section (nb)=";
      unsigned int curLine = 0;
      string line;
      while(getline(fileInput, line))  // I changed this, see below
	{
	  curLine++;
	  if (line.find(search, 0) != string::npos) 
	    {
	      stringstream ss(line); 
	      string buf; 
	      vector<string> tokens;
	      while (ss >> buf)
		tokens.push_back(buf);
	      cout << "found: " << search << "line: " << curLine << endl;
	      cout << "XS=" << tokens[tokens.size()-1] <<endl;
	      fileInput.close();
	      return atof(tokens[tokens.size()-1].c_str());
	    }
	}
      fileInput.close();
      cout<< "not found :(" <<endl;
      return 0;
    }
    
    float getGetGenFiltEffFromLog()
    {
      cout << "getting GenFiltEff from log" << endl;
      string cfilename="log.generate";
      std::ifstream fileInput;
      fileInput.open(cfilename.c_str());
      char* search = "MetaData: GenFiltEff =";
      unsigned int curLine = 0;
      string line;
      while(getline(fileInput, line)) 
	{
	  curLine++;
	  if (line.find(search, 0) != string::npos) 
	    {
	      stringstream ss(line); 
	      string buf; 
	      vector<string> tokens;
	      while (ss >> buf)
		tokens.push_back(buf);
	      cout << "found: " << search << "line: " << curLine << endl;
	      cout << "GenFiltEff=" << tokens[tokens.size()-1] <<endl;
	      fileInput.close();
	      return atof(tokens[tokens.size()-1].c_str());
	    }
	}
      fileInput.close();
      cout<< "not found :(" <<endl;
      return 1.0;
    }
        
    void init() 
    {
      std::cout << "**** I'm in init() Rivet function" << std::endl;
      
      file = new TFile("TreeJets.root","recreate");

      tree = new TTree("TreeJets","A Tree with jets");


      h_nFiles       = new TH1D("nFiles",       "Number of files",      1, 0, 2);
      h_xs           = new TH1D("xs",           "Total cross section",  1, 0, 2);
      h_nEvents      = new TH1D("nEvents",      "Number of events",     1, 0, 2);
      h_sumOfWeights = new TH1D("sumOfWeights", "Sum of event weights", 1, 0, 2);
      h_genFiltEff   = new TH1D("genFiltEff",   "Filter efficiency",    1, 0, 2);

      jet_N   = new int[nAlg];
      jet_E   = new vector<double>[nAlg];
      jet_Pt  = new vector<double>[nAlg];
      jet_M   = new vector<double>[nAlg];                                                                                                         
      jet_Rap = new vector<double>[nAlg];                                                                                            
      jet_Eta = new vector<double>[nAlg];                                          
      jet_Phi = new vector<double>[nAlg];


      const FinalState fs(Cuts::abseta < 10);/*-5.0, 5.0, 0.);*/

      for(int iAlg=0; iAlg<nAlg; iAlg++) 
	{
	  cout << "nAlg = " << iAlg << endl;
	  FastJets j( fs, FastJets::ANTIKT, R[iAlg] );
	  j.useInvisibles(true);
	  declare(j,AlgName[iAlg]);//addProjection(j,AlgName[iAlg]);
	}
      
      tree->Branch("eventNumber",&eventNumber);
      tree->Branch("eventWeight",&eventWeight);

      for(int iAlg=0; iAlg<nAlg; iAlg++) 
	{	  
	  tree->Branch( (AlgName[iAlg]+string("_jet_N")).  c_str(), &jet_N  [iAlg] );
	  tree->Branch( (AlgName[iAlg]+string("_jet_E")).  c_str(), &jet_E  [iAlg] );
	  tree->Branch( (AlgName[iAlg]+string("_jet_M")).  c_str(), &jet_M  [iAlg] );
	  tree->Branch( (AlgName[iAlg]+string("_jet_Pt")). c_str(), &jet_Pt [iAlg] );
	  tree->Branch( (AlgName[iAlg]+string("_jet_Rap")).c_str(), &jet_Rap[iAlg] );
	  tree->Branch( (AlgName[iAlg]+string("_jet_Eta")).c_str(), &jet_Eta[iAlg] );
	  tree->Branch( (AlgName[iAlg]+string("_jet_Phi")).c_str(), &jet_Phi[iAlg] );
	}
      evt  = 0;
      sumw = 0;
      
      std::cout << "**** Finished with init()" << std::endl;
    }
    

    
    void analyze(const Event& event) 
    {
      /// First, clear all variables for each new event.
      for(int iAlg=0; iAlg<nAlg; iAlg++) 
	{
	  eventNumber = 0;
	  eventWeight = 0.0;

	  jet_N  [iAlg] = 0;

	  jet_E  [iAlg].clear();
	  jet_M  [iAlg].clear();
	  jet_Pt [iAlg].clear();
	  jet_Rap[iAlg].clear();
	  jet_Eta[iAlg].clear();
	  jet_Phi[iAlg].clear();
	}


      double cut_jet_pt = 10.0;
      double cut_jet_y  = 10.0;
      
      evt++;
      if ( evt%1000 == 0 ) 
	cout<<evt<<endl;


      eventNumber = evt;
      eventWeight = event.weight();

      sumw = sumw + eventWeight;

      Jets jets[nAlg];
      for( size_t iAlg=0; iAlg < nAlg; iAlg++ ) 
	{
	  const Jets _jets = apply<FastJets>(event, AlgName[iAlg]).jetsByPt(Cuts::pT > cut_jet_pt*GeV && Cuts::absrap < cut_jet_y);
	  for ( size_t ijet=0; ijet<_jets.size(); ijet++)
	    {
	      FourMomentum jmom = _jets[ijet].momentum();
              if ( fabs(jmom.rapidity()) < cut_jet_y )
                {
                  jets[iAlg].push_back(_jets[ijet]);
		}
	    }
	}
	     
      for( size_t iAlg=0; iAlg<nAlg; iAlg++)
	{
	  jet_N[iAlg] = 0;
	  jet_N[iAlg] = jets[iAlg].size(); 
	  
	  if (jet_N[iAlg] < 1 ) 
	    continue;
	  
	  for ( size_t iJet=0 ; iJet < jet_N[iAlg]; iJet++ )
	    {
	      FourMomentum jmom = jets[iAlg][iJet].momentum();
	      jet_E  [iAlg].push_back( jmom.E()    / GeV );
	      jet_M  [iAlg].push_back( jmom.mass() / GeV );
	      jet_Pt [iAlg].push_back( jmom.pT()   / GeV );
	      jet_Eta[iAlg].push_back( jmom.eta() );
	      jet_Phi[iAlg].push_back( jmom.phi() );
	      jet_Rap[iAlg].push_back( jmom.rapidity() );
	      
	    }
	}
      tree->Fill();      
    }


    void finalize() 
    {
      
      std::cout << "I'm in finalize() function" << std::endl;
      std::cout << "CrossSection: " << crossSection() << std::endl;

      double XSection=1.0;
      XSection=getXSfromLog();
      cout << "Using cross section: " << XSection << endl;
      
      double genFiltEff=1.0;
      genFiltEff=getGetGenFiltEffFromLog();
      cout << "Using gen filter efficiency: : " << genFiltEff << endl;
      
      h_nFiles       -> Fill( 1, 1);
      h_xs           -> Fill( 1, XSection );
      h_nEvents      -> Fill( 1, evt );
      h_sumOfWeights -> Fill( 1, sumw );
      h_genFiltEff   -> Fill( 1, genFiltEff );


      file -> Write();
      file -> Close();

    }
    
    
    
  };
    
  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(JETS);
}

