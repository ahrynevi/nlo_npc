import os
import shutil

### To get the list of files to be saved do:
### for i in `ls`; do echo "'"${i}"',";done
###


###
### Check carefully this list of file to be saved, others will be removed
###

doNotRemove=[
    '100000',
    '364700',
    '364701',
    '364702',
    '364703',
    '364704',
    '364705',
    '364706',
    '364707',
    '364708',
    '364709',
    '.git',
    'GenerateJO.py',
    'GenerateJO_PY8_EIG.py',
    'GenerateJONew_PY8_EIG.py',
    'LeadingJets_WithSD.cc',
    'LeadingJets_WithSD_check.cc',
    'runProd.sh',
    'rivetenv.sh',
    'group.phys-gener.powheg_000311.361284.jj_A14NNPDF23_JZ4_13TeV.TXT.mc15_v1._00001.tar.gz',
    'Generators',
    'MC15JobOptions',
    'InstallArea',
    'README',
    'Matching.submit',
    'Matching.sh',
    'Matching.C',
    'RivetJETS.so',
    'rivet-buildplugin_local',
    'Result',
    'logjets.txt',
    'logjets2.txt',
    'logparticle.txt',
    'logparticle2.txt',
    'InclusiveJets.cc',
    'LeadingJets.cc',
    'clean.py',
    'runBatchArray.sh',
    'setup.sh',
    'setupCT10.sh',
    'submitBatch.sh',
    'yoda2root.py',
    'setFilterEfficiency.C',
    'share',
    'monitoring.sh',
    'myjob.submit',
    'myjobPowheg.submit',
    'HTCrunBatch.sh',
    'logs',
    'Results',
    'macroes',
    'BatchOutput'
    ]
allFilesInDir=os.listdir(".")





def notification():
    yes = set(['yes','y', 'ye'])
    no  = set(['no','n'])
    print "Are you sure you want to remove everything except: ", doNotRemove
    print "yes/no ?\n"
    choice = raw_input().lower()
    if choice in yes:
        print "Removing....\n"
        return True
    elif choice in no:
        print "Exiting. No action\n"
        exit(-1)
    else:
        notification()
        
notification()

for i in allFilesInDir:
    if i not in doNotRemove:
        try:
            os.remove(str(i))
        except EnvironmentError:
            shutil.rmtree(str(i))
        except Exception,e:
            print e, "No action"
            
print "Removed.\n"
