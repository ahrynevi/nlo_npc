## These variables need to exist
prefix=/cvmfs/atlas.cern.ch/repo/sw/software/21.6/sw/lcg/releases/MCGenerators/rivet/3.1.0-3b775/x86_64-slc6-gcc62-opt
exec_prefix=${prefix}
datarootdir=${prefix}/share

## Try to automatically work out the YODA Python path
YODA_PYTHONPATH="/cvmfs/atlas.cern.ch/repo/sw/software/21.6/sw/lcg/releases/MCGenerators/yoda/1.8.0-e27f2/x86_64-slc6-gcc62-opt/lib/python2.7/site-packages"
test -n "$YODA_PYTHONPATH" || { (which yoda-config > /dev/null) && YODA_PYTHONPATH=`yoda-config --pythonpath`; }
test -n "$YODA_PYTHONPATH" || echo "yoda-config could not be found: you may need to manually set paths to libYODA and the yoda Python package" 1>&2

export PATH="$exec_prefix/bin:/cvmfs/atlas.cern.ch/repo/sw/software/21.6/sw/lcg/releases/MCGenerators/yoda/1.8.0-e27f2/x86_64-slc6-gcc62-opt/lib/../bin:$PATH"
export LD_LIBRARY_PATH="${exec_prefix}/lib:/cvmfs/atlas.cern.ch/repo/sw/software/21.6/sw/lcg/releases/MCGenerators/yoda/1.8.0-e27f2/x86_64-slc6-gcc62-opt/lib:/cvmfs/atlas.cern.ch/repo/sw/software/21.6/sw/lcg/releases/HepMC/2.06.09-0a23a/x86_64-slc6-gcc62-opt/lib:/cvmfs/atlas.cern.ch/repo/sw/software/21.6/sw/lcg/releases/fastjet/3.2.0-bb0d1/x86_64-slc6-gcc62-opt/lib:/cvmfs/atlas.cern.ch/repo/sw/software/21.6/sw/lcg/releases/fjcontrib/1.041-92892/x86_64-slc6-gcc62-opt/lib:$LD_LIBRARY_PATH"
export PYTHONPATH="/cvmfs/atlas.cern.ch/repo/sw/software/21.6/sw/lcg/releases/MCGenerators/rivet/3.1.0-3b775/x86_64-slc6-gcc62-opt/lib/python2.7/site-packages:$YODA_PYTHONPATH:$PYTHONPATH"

export TEXMFHOME="${datarootdir}/Rivet/texmf:$TEXMFHOME"
export HOMETEXMF="${datarootdir}/Rivet/texmf:$HOMETEXMF"
export TEXMFCNF="${datarootdir}/Rivet/texmf/cnf:$TEXMFCNF"
export TEXINPUTS="${datarootdir}/Rivet/texmf/tex//:$TEXINPUTS"
export LATEXINPUTS="${datarootdir}/Rivet/texmf/tex//:$LATEXINPUTS"

if (complete &> /dev/null); then
    test -e "${datarootdir}/Rivet/rivet-completion" && source "${datarootdir}/Rivet/rivet-completion"
fi

unset YODA_PYTHONPATH
